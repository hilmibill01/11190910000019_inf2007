package id.hilmibill01.gitar.controller;

import com.google.gson.Gson;
import id.hilmibill01.gitar.model.Gitar;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author USER
 */
public class GitarController {

    private static final String FILE = "D:\\Gitar.json";
    private Gitar gitar;
    private final Scanner in;
    private String noOrder;
    private int jenisGitar;
    private final LocalDateTime waktuPemesanan;
    private LocalDateTime waktuPembayaran;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public GitarController() {
        in = new Scanner(System.in);
        waktuPemesanan = LocalDateTime.now();
        waktuPembayaran = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setPemesanan() {
        System.out.println("Masukkan NoOrder");
        noOrder = in.next();

        String formatWaktuPemesanan = waktuPemesanan.format(dateTimeFormat);
        System.out.println("WaktuPemesanan : " + formatWaktuPemesanan);

        System.out.println("DAFTAR JENIS GITAR");
        System.out.println("  1. Fender Stratocester");
        System.out.println("  2. Gipson Les Paul");
        System.out.println("  3. Godin Multiac ACS-SA");
        System.out.println("  4. Schecter Synyster Gates");
        System.out.println("  5. Samick Accoustic");
        System.out.println("  6. Taylor Accustic");
        System.out.println("  7. Yamaha Accoustic");
        System.out.println("  8. Takamine Classical");
        System.out.println("  9. Epiphone");
        System.out.println(" 10. Norman Rodriguez");
        System.out.println("===========T=O=K=O=====G=U=I=T=A=R=====H=I=L=M=I==============");
        System.out.println("Pilih Jenis Gitar");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.println("Masukkan Jenis Gitar");
        }
        jenisGitar = in.nextInt();

        gitar = new Gitar();
        gitar.setNoOrder(noOrder.toUpperCase());
        gitar.setWaktuPemesanan(waktuPemesanan);
        gitar.setJenisGitar(jenisGitar);

        setWriteGitar(FILE, gitar);

        System.out.println("Apakah Mau Input Kembali?");
        System.out.print("1) ya, 2) Tidak :");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPemesanan();
        }
    }

    /**
     * Method untuk menyimpan data gitar ke file berbentuk json
     *
     * @param file, tipedata <code>String</code> merujuk ke file yang akan
     * disimpan.
     * @param parkir, model <code>Parkir</code> yang akan disimpan.
     */
    public void setWriteGitar(String file, Gitar gitar) {
        Gson gson = new Gson();

        List<Gitar> gitars = getReadGitar(file);
        gitars.remove(gitar);
        gitars.add(gitar);

        String json = gson.toJson(gitars);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(GitarController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method untuk menginput data keluar parkir
     */
    public void setPembayaran() {
        System.out.println("Masukkan NoOrder : ");
        noOrder = in.next();

        Gitar p = getSearch(noOrder);
        if (p != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(p.getWaktuPemesanan());
            waktuPembayaran = LocalDateTime.now();
            long hari = tempWaktu.until(waktuPembayaran, ChronoUnit.DAYS);
            p.setWaktuPembayaran(waktuPembayaran);

            if (p.getJenisGitar() == 1) {
                biaya = new BigDecimal(11000000);
            } else if (p.getJenisGitar() == 2) {
                biaya = new BigDecimal(15000000);
            } else if (p.getJenisGitar() == 3) {
                biaya = new BigDecimal(10000000);
            } else if (p.getJenisGitar() == 4) {
                biaya = new BigDecimal(2000000);
            } else if (p.getJenisGitar() == 5) {
                biaya = new BigDecimal(4500000);
            } else if (p.getJenisGitar() == 6) {
                biaya = new BigDecimal(5500000);
            } else if (p.getJenisGitar() == 7) {
                biaya = new BigDecimal(35000000);
            } else if (p.getJenisGitar() == 8) {
                biaya = new BigDecimal(7500000);
            } else if (p.getJenisGitar() == 9) {
                biaya = new BigDecimal(16000000);
            } else if (p.getJenisGitar() == 10) {
                biaya = new BigDecimal(18000000);
            }
            p.setBiaya(biaya);
            p.setBayar(true);

            System.out.println("No Order : " + p.getNoOrder());
            System.out.println("JenisGitar : " + p.getJenisGitar());
            System.out.println("Waktu Pemesanan : " + p.getWaktuPemesanan().format(dateTimeFormat));
            System.out.println("Waktu Pembayaran : " + p.getWaktuPembayaran().format(dateTimeFormat));
            System.out.println("Biaya : " + biaya);

            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWriteGitar(FILE, p);
                    break;
                case 2:
                    setPembayaran();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setPembayaran();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setPembayaran();
        }
    }

    public void getDataGitar() {
        List<Gitar> gitars = getReadGitar(FILE);
        Predicate<Gitar> isBayar = e -> e.isBayar() == true;
        Predicate<Gitar> isDate = e -> e.getWaktuPembayaran().toLocalDate().equals(LocalDate.now());

        List<Gitar> pResults = gitars.stream().filter(isBayar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Gitar::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("No Order \tWaktu Pemesanan \tWaktu Pembayaran \t\tHarga");
        System.out.println("--------- \t----------- \t\t------------ \t\t\t-----");
        pResults.forEach((p) -> {
            System.out.println(p.getNoOrder() + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat) + "\t" + p.getWaktuPembayaran().format(dateTimeFormat) + "\t\t" + p.getBiaya());
        });
        System.out.println("--------- \t----------- \t\t------------ \t\t\t-----");
        System.out.println("====================================");
        System.out.println("Harga Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataGitar();
        }
    }

    public List<Gitar> getReadGitar(String file) {
        List<Gitar> gitars = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Gitar[] ps = gson.fromJson(line, Gitar[].class);
                gitars.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GitarController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GitarController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return gitars;
    }

    private Gitar getSearch(String noOrder) {
        List<Gitar> gitars = getReadGitar(FILE);
        Gitar p = gitars.stream()
                .filter(pp -> noOrder.equalsIgnoreCase(pp.getNoOrder()))
                .findAny()
                .orElse(null);

        return p;
    }
}

//            waktuPembayaran = LocalDateTime.now();
//            if (h.get() > 1) {
//                biaya = new BigDecimal(h./getHari());
//                if (h.getHari() > 1) {
//                    biaya = new BigDecimal(h.getHari());
//                    if (h.getTipe() == 1) {
//                        biaya = biaya.multiply(new BigDecimal(5000000));
//                    } else if (h.getTipe() == 2) {
//                        biaya = biaya.multiply(new BigDecimal(1000000));
//                    } else if (h.getTipe() == 3) {
//                        biaya = biaya.multiply(new BigDecimal(3000000));
//                    } else if (h.getTipe() == 4) {
//                        biaya = biaya.multiply(new BigDecimal(7000000));
//                    } else if (h.getTipe() == 5) {
//                        biaya = biaya.multiply(new BigDecimal(8000000));
//                    } else if (h.getTipe() == 6) {
//                        biaya = biaya.multiply(new BigDecimal(4000000));
//                    } else if (h.getTipe() == 7) {
//                        biaya = biaya.multiply(new BigDecimal(2500000));
//                    } else if (h.getTipe() == 8) {
//                        biaya = biaya.multiply(new BigDecimal(5600000));
//                    } else if (h.getTipe() == 9) {
//                        biaya = biaya.multiply(new BigDecimal(19000000));
//                    } else if (h.getTipe() == 10) {
//                        biaya = biaya.multiply(new BigDecimal(7500000));
//                    }else{
//                        System.out.println("harga tidak tersedia");
//                    }   
//                } else {
//                    hari = 1
//                }
//                
//            }
//            }
//        }
//    void getLaporanTransaksi() {
//
//    }

