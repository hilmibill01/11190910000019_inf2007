package id.hilmibill01.gitar.controller;

import id.hilmibill01.gitar.model.Info;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Pemesanan");
        System.out.println("2. Menu Pembayaran");
        System.out.println("3. Laporan Transaksi");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("========== T O K O ===== G I T A R ===== H I L M I ==========");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        GitarController pc = new GitarController();
        switch (noMenu) {
            case 1:
                pc.setPemesanan();
                break;
            case 2:
                pc.setPembayaran();
                break;
            case 3:
                pc.getDataGitar();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
