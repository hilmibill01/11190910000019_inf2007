package id.hilmibill01.gitar.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAccessor;

/**
 *
 * @author USER
 */
public class Gitar {
    private static final long serialVersionUID = -6756463875294313469L;

    private String noOrder;
    private int jenisGitar;
    private LocalDateTime waktuPemesanan;
    private LocalDateTime waktuPembayaran;
    private BigDecimal biaya;
    private boolean bayar = false;

    public Gitar() {

    }

    public Gitar(String noOrder, int jenis, LocalDateTime waktuPemesanan, LocalDateTime waktuPembayaran, BigDecimal biaya) {
        this.noOrder = noOrder;
        this.jenisGitar = jenisGitar;
        this.waktuPemesanan = waktuPemesanan;
        this.waktuPembayaran = waktuPembayaran;
        this.biaya = biaya;
    }

    public String getNoOrder() {
        return noOrder;
    }

    public void setNoOrder(String noOrder) {
        this.noOrder = noOrder;
    }

    public int getJenisGitar() {
        return jenisGitar;
    }

    public void setJenisGitar(int jenisGitar) {
        this.jenisGitar = jenisGitar;
    }

    public LocalDateTime getWaktuPemesanan() {
        return waktuPemesanan;
    }

    public void setWaktuPemesanan(LocalDateTime waktuPemesanan) {
        this.waktuPemesanan = waktuPemesanan;
    }

    public LocalDateTime getWaktuPembayaran() {
        return waktuPembayaran;
    }

    public void setWaktuPembayaran(LocalDateTime waktuPembayaran) {
        this.waktuPembayaran = waktuPembayaran;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isBayar() {
        return bayar;
    }

    public void setBayar(boolean bayar) {
        this.bayar = bayar;
    }

    

    
    @Override
    public String toString() {
        return "Gitar{" + "noOrder=" + noOrder + ", jenisGitar=" + jenisGitar + ", waktuPemesanan=" + waktuPemesanan + ", waktuPembayaran=" + waktuPembayaran + ", biaya=" + biaya + ", bayar=" + bayar + '}';
    }
}
