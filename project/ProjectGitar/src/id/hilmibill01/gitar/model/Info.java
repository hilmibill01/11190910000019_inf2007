package id.hilmibill01.gitar.model;

/**
 *
 * @author USER
 */
public class Info {
    private final String aplikasi = "Aplikasi Jual Beli Gitar";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
