package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class UrutanBilangan {
    public static void main(String[] args) {
        int bilBul1, bilBul2, bilBul3;
        
        Scanner bilangan = new Scanner(System.in);
        System.out.println("Masukkan Nilai Bilangan Bulat Positif 1 :");
        bilBul1 = bilangan.nextInt();
        System.out.println("Bilangan Bulat 1 : " + bilBul1);
        System.out.println("\nMasukkan Nilai Bilangan Bulat Positif 2 :");
        bilBul2 = bilangan.nextInt();
        System.out.println("Bilangan Bulat 2 : " + bilBul2);
        System.out.println("\nMasukkan Nilai Bilangan Bulat Positif 3 :");
        bilBul3 = bilangan.nextInt();
        System.out.println("Bilangan Bulat 3 : " + bilBul3);
        
        if (bilBul1 <= bilBul2 && bilBul2 <= bilBul3) {
            System.out.println(bilBul1 + "," + bilBul2 + "," + bilBul3);
        } else if (bilBul1 <= bilBul3 && bilBul3 <= bilBul2) {
            System.out.println(bilBul1 + "," + bilBul3 + "," + bilBul2);
        } else if (bilBul2 <= bilBul1 && bilBul1 <= bilBul3) {
            System.out.println(bilBul2 + "," + bilBul1 + "," + bilBul3);
        } else if (bilBul2 <= bilBul3 && bilBul3 <= bilBul1) {
            System.out.println(bilBul2 + "," + bilBul3 + "," + bilBul1);
        } else if (bilBul3 <= bilBul1 && bilBul1 <= bilBul2) {
            System.out.println(bilBul3 + "," + bilBul1 + "," + bilBul2);
        } else {
            System.out.println("\nBilangan terurut dari yang terkecil adalah " + bilBul3 + "," + bilBul2 + "," + bilBul1);
        }
        
    }
}