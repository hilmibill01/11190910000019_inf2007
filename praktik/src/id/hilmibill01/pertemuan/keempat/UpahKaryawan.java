package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class UpahKaryawan {
    public static void main(String[] args) {
        final int jamNormal = 48; // jumlah jam kerja normal perminggu
        final float upahPerJam = 2000; // upah per jam
        final float upahLembur = 3000; // upah lembur per jam
        String nama;
        int JJK; // jumlah jam kerja
        float lembur, upah;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nama : ");
        nama = input.nextLine();
        System.out.println("Masukkan jumlah jam kerja : ");
        JJK = input.nextInt();
        
        System.out.println("Nama " + nama + ", Jumlah jam kerja : " + JJK + " jam");
        if (JJK <= jamNormal) {
            upah = JJK * upahPerJam;
        } else {
            lembur = JJK - jamNormal;
            upah = (jamNormal * upahPerJam) + (lembur * upahLembur);
        }
        System.out.println("Nama " + nama + " Mendapatkan gaji sebesar Rp " + upah);
    }
}
