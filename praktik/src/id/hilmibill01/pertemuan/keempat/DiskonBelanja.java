package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class DiskonBelanja {
    public static void main(String[] args) {
        int totalBelanja, diskon, setelahDiskon;

        Scanner total = new Scanner(System.in);
        System.out.println("Masukkan jumlah total belanja : ");
        totalBelanja = total.nextInt();
        if (totalBelanja > 120000) {
            System.out.println("Total belanja yang melebihi harga "
                    + "dari 120000 mendapatkan diskon 7%");
            diskon = (totalBelanja * 7) / 100;
            setelahDiskon = totalBelanja - diskon;
            System.out.println("Anda mendapat diskon harga sebanyak = " + diskon);
            System.out.println("Total Belanja Anda menjadi = " + setelahDiskon);
        }
    }
}