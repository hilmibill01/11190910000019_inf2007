package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class NilaiMahasiswa {
    public static void main(String[] args) {
        float nilai;
        char indeks;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai : ");
        nilai = input.nextFloat();
        
        if (nilai >= 80) {
            indeks = 'A';
        } else {
            if (nilai >= 70 && nilai < 80) {
                indeks = 'B';
            } else {
                if (nilai >= 55 && nilai < 70) {
                    indeks = 'C';
                } else {
                    if (nilai >= 40 && nilai < 55) {
                        indeks = 'D';
                    } else {
                        indeks = 'E';
                    }
                }
            }
        }
        System.out.println("Predikat dari nilai anda adalah " + indeks);
    }
}