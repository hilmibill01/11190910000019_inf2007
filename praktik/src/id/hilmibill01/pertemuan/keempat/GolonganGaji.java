package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class GolonganGaji {
    public static void main(String[] args) {
        final int jamNormal = 48;
        final float upahLembur = 3000;
        
        String nama;
        char gol;
        int jamLembur, JJK;
        float upahPerjam = 0, upahTotal;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nama anda : ");
        nama = input.nextLine();
        System.out.println("Masukkan jumlah jam kerja");
        JJK = input.nextInt();
        System.out.println("Gol A = 4000, Gol B = 5000, Gol C = 6000, Gol D = 7500");
        System.out.println("Masukkan golongan upah : ");
        gol = input.next().charAt(0);
        
        if (gol == 'A') {
            upahPerjam = 4000;
        } else {
            if (gol == 'B') {
                upahPerjam = 5000;
            } else {
                if (gol == 'C') {
                    upahPerjam = 6000;
                } else {
                    if (gol == 'D') {
                        upahPerjam = 7500;
                    }
                }
            }
        }
        
        if (JJK <= jamNormal) {
            upahTotal = JJK * upahPerjam;
        } else {
            jamLembur = JJK - jamNormal;
            upahTotal = (jamNormal*upahPerjam) + (jamLembur*upahLembur);
        }
        System.out.println("Nama " + nama + " gaji anda sebesar Rp " + upahTotal);
    }
}

