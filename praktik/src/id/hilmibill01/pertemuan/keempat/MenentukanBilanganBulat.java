package id.hilmibill01.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class MenentukanBilanganBulat {
    public static void main(String[] args) {
        int bilangan;
        
        Scanner bil = new Scanner(System.in);
        System.out.println("Masukkan Bilangan Bulat : ");
        bilangan = bil.nextInt();
        
        System.out.println("Bilangan yang diinput adalah " + bilangan);
        if (bilangan > 0) {
            System.out.println("Bilangan Positif");
        } else {
            if (bilangan < 0) {
                System.out.println("Bilangan Negatif");
            } else {
                if (bilangan == 0) {
                    System.out.println("Nol");
                }
            }
        }
    }
}
