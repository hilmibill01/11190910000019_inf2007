package id.hilmibill01.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Hilmi
 */
public class SelectionSort {
    public int[] getSelectionSortMaks (int [] L, int n) {
        int i,j,imaks,temp;
        for (i = n-1; i > 0; i--){
            imaks = 1;
            for (j = 0; j <= i; j++){
                if (L[j] > L[imaks]) {
                    imaks = j;                    
                }
                
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        } 
        return L;
    }
    public static void main(String[] args) {
        int L[] = { 29, 27, 10, 8, 76, 21 };
        int n = L.length;
        SelectionSort app = new SelectionSort();
        System.out.println("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMaks(L, n);
        System.out.println("Bilangan yang sudah terurut: ");
        System.out.println(Arrays.toString(L));
    }

    void getSelectionSortMax(int[] L, int n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
