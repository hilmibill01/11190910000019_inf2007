package id.hilmibill01.pertemuan.kelima;

/**
 *
 * @author Hilmi
 */
public class CetakBanyakHelloWorldRepeat {

    public static void main(String[] args) {
        int i = 1;

        do {
            System.out.println("Hello, World");
            i++;
        } while (i <= 10);
    }
}
