package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class ValidasiSandiWhile {
    
   public static void main(String[] args) {
        final String password = "abc123";
        String sandi;
        boolean sah;
        int count;
        
        count = 1;
        sah = false;
        while ((!sah) && (count <= 3)) {
            Scanner in = new Scanner(System.in);
            System.out.println("Masukkan kata sandi anda ");
            sandi = in.nextLine();
            if (sandi == null ? password == null : sandi.equals(password)) {
                sah = true;
            } else {
                count = count + 1;
            }
        }
    }
}
