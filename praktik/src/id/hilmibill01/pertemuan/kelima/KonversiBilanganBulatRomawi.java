package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */

public class KonversiBilanganBulatRomawi {

    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        int bil = 0;

        while (bil != -1) {
            System.out.println("Masukkan angka yang ingin dikonversi = ");
            bil = masukan.nextInt();
            if (bil > 0 && bil < 500000000) {
                while (bil >= 1000) {
                    System.out.print("M");
                    bil = bil - 1000;
                }
                if (bil >= 500) {
                    if (bil >= 900) {
                        System.out.print("CM");
                        bil = bil - 900;
                    } else {
                        System.out.print("D");
                        bil = bil - 500;
                    }
                }
                while (bil >= 100) {
                    if (bil >= 400) {
                        System.out.print("CD");
                        bil = bil - 400;
                    } else {
                        System.out.print("C");
                        bil = bil - 100;
                    }
                }
                if (bil >= 50) {
                    if (bil >= 90) {
                        System.out.print("XC");
                        bil = bil - 90;
                    } else {
                        System.out.print("L");
                        bil = bil - 50;
                    }
                }
                while (bil >= 10) {
                    if (bil >= 40) {
                        System.out.print("XL");
                        bil = bil - 40;
                    } else {
                        System.out.print("X");
                        bil = bil - 10;
                    }
                }
                if (bil >= 5) {
                    if (bil == 9) {
                        System.out.print("IX");
                        bil = bil - 9;
                    } else {
                        System.out.print("V");
                        bil = bil - 5;
                    }
                }
                while (bil >= 1) {
                    if (bil == 4) {
                        System.out.print("IV");
                        bil = bil - 4;
                    } else {
                        System.out.print("I");
                        bil = bil - 1;
                    }
                }
            } else {
                System.out.println("Program selesai");
            }
            System.out.println("");
        }
    }
}
