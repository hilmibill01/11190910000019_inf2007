package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class HitungRerataWhile {

    public static void main(String[] args) {
        int i, x, jumlah;
        float rerata;
        i = 0;
        jumlah = 0;
        Scanner in = new Scanner(System.in);
        x = in.nextInt();
        while (x != -1) {
            i = i + 1;
            jumlah = jumlah + x;
            x = in.nextInt();
        }
        if (i != 0) {
            rerata = (float) jumlah / i;
            System.out.println(rerata);
        } else {

        }
    }
}
