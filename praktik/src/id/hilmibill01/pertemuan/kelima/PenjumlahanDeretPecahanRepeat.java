package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class PenjumlahanDeretPecahanRepeat {

    public static void main(String[] args) {
        int x;
        float s = 0;

        Scanner in = new Scanner(System.in);
        System.out.print("x = ");
        x = in.nextInt();
        do {
            s = s + (float) 1 / x;
            System.out.print("x = ");
            x = in.nextInt();
        } while (x != -1);
        System.out.println("jumlah = " + s);
    }
}
