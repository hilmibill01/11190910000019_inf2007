package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class CariMinimum {

    public static void main(String[] args) {
        int n, x, min, i;

        Scanner in = new Scanner(System.in);
        System.out.print("n = ");
        n = in.nextInt();
        System.out.print("bilangan = ");
        x = in.nextInt();
        min = x;
        for (i = 2; i <= n; i++) {
            System.out.print("bilangan = ");
            x = in.nextInt();
            if (x < min) {
                min = x;
            }
        }
        System.out.println("nilai terkecil = " + min);
    }
}
