package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class Perpangkatan {

    public static void main(String[] args) {
        int n, i;
        float a, p;

        Scanner in = new Scanner(System.in);
        System.out.print("angka = ");
        a = in.nextInt();
        System.out.print("pangkat = ");
        n = in.nextInt();
        p = 1;
        for (i = 1; i <= n; i++) {
            p = p * a;
        }
        System.out.println("hasil = " + p);
    }
}
