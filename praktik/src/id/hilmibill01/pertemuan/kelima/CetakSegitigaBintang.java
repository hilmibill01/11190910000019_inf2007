package id.hilmibill01.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class CetakSegitigaBintang {

    public static void main(String[] args) {
        int n, i, a;

        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan jumlah banyaknya baris ");
        n = input.nextInt();

        for (i = 1; i <= n; i++) {
            for (a = 1; a <= i; a++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
