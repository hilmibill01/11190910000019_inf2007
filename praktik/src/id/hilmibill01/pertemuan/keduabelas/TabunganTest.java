package id.hilmibill01.pertemuan.keduabelas;

/**
 *
 * @author Hilmi
 */
public class TabunganTest {

    public static void main(String[] args) {
        Tabungan t = new Tabungan (5000);
        System.out.println("saldo awal" + " "+ t.saldo);
        t.ambilUang(2300);
        System.out.println("Jumlah Uang yang diambil : 2300");
        System.out.println("saldo sekarang : " + t.saldo);    
        }
    }
