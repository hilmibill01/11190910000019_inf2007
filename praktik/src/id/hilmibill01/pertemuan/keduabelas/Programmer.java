package id.hilmibill01.pertemuan.keduabelas;

/**
 *
 * @author Hilmi
 */
public class Programmer extends Pegawai {

    private int bonus;
    private int tunjangan;
    private int gaji;

    public Programmer(String nama, int gaji, int tunjangan) {
        this.nama = nama;
        this.gaji = gaji;
        this.tunjangan = tunjangan;
    }

    public int infoGaji() {
        return this.gaji;
    }

    public int infoBonus() {
        return this.bonus + tunjangan;
    }
}
