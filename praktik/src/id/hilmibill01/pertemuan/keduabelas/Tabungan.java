package id.hilmibill01.pertemuan.keduabelas;

/**
 *
 * @author Hilmi
 */
public class Tabungan {

    public int saldo;

    public Tabungan(int saldo) {
        this.saldo = saldo;
        System.out.println("Saldo" + saldo);
    }

    public int ambilUang(int jumlah) {
        saldo = saldo - jumlah;
        return saldo;
    }

}
