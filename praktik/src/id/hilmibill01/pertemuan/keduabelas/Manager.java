package id.hilmibill01.pertemuan.keduabelas;

/**
 *
 * @author Hilmi
 */
public class Manager extends Pegawai {

    private int tunjangan;

    public Manager(String nama, int gaji, int tunjangan) {
        this.nama = nama;
        this.gaji = gaji;
        this.tunjangan = tunjangan;
    }

    public int infoGaji() {
        return this.gaji;
    }

    public int infoTunjangan() {
        return this.tunjangan;
    }
}
