package id.hilmibill01.pertemuan.keduabelas;

/**
 *
 * @author Hilmi
 */
public class PenyimpananUang extends Tabungan {

    private double tingkatbunga;

    public PenyimpananUang(int saldo, double tingkatbunga) {
        super(saldo);
        this.tingkatbunga = tingkatbunga;
    }

    public double cekUang() {
        return saldo + (saldo * tingkatbunga);

    }

    String CekUang() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
