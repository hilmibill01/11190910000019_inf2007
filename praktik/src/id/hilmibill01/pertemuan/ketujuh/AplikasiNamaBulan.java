package id.hilmibill01.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class AplikasiNamaBulan {
    
    public static void main(String[] args) {
        int noBulan;
        
        Scanner in = new Scanner(System.in);
        System.out.print("no bulan: ");
        noBulan = in.nextInt();
        
        NamaBulan namaBulan = new NamaBulan();
        System.out.println(namaBulan.getNama(noBulan));
    }
}
