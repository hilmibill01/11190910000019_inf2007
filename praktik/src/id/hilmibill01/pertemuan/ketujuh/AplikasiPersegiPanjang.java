package id.hilmibill01.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class AplikasiPersegiPanjang {

    public static void main(String[] args) {
        // mencoba kontraktor default
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();

        Scanner in = new Scanner(System.in);
        double panjang, lebar;

        System.out.print("masukkan panjang: ");
        panjang = in.nextDouble();

        System.out.print("masukkan lebar: ");
        lebar = in.nextDouble();

        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());

    }
}
