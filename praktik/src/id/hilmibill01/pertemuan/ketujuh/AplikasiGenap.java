package id.hilmibill01.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class AplikasiGenap {
    
    public static void main(String[] args) {
        int bilangan;

        Scanner in = new Scanner(System.in);
        System.out.print("bilangan: ");
        bilangan = in.nextInt();

        Genap genap = new Genap();
        if (genap.getHasil(bilangan)) {
            System.out.println("Genap");
        } else {
            System.out.println("Ganjil");
        }
    }
}
