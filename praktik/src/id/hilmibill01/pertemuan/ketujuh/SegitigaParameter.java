package id.hilmibill01.pertemuan.ketujuh;

/**
 *
 * @author Hilmi
 */
public class SegitigaParameter {

    double luas;

    public SegitigaParameter(double alas, double tinggi) {
        luas = (alas * tinggi) / 2;
        System.out.println("Luas = " + luas);
    }
}
