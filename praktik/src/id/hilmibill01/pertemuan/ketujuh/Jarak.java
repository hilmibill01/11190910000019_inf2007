package id.hilmibill01.pertemuan.ketujuh;

import static java.lang.Math.sqrt;
import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class Jarak {
    
    public int x1, x2, y1, y2;

    public double getJarak(double Jarak) {
        Scanner in = new Scanner(System.in);
        System.out.print("x1 = ");
        x1 = in.nextInt();
        System.out.print("x2 = ");
        x2 = in.nextInt();
        System.out.print("y1 = ");
        y1 = in.nextInt();
        System.out.print("y2 = ");
        y2 = in.nextInt();

        Jarak = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        return Jarak;
    }
}
