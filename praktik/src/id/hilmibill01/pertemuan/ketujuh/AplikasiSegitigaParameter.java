package id.hilmibill01.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class AplikasiSegitigaParameter {
    
    public static void main(String[] args) {
        int i, N;
        double a, t;

        Scanner in = new Scanner(System.in);
        System.out.print("N: ");
        N = in.nextInt();

        for (i = 1; i <= N; i++) {
            System.out.print("masukkan alas: ");
            a = in.nextDouble();
            System.out.print("maukkan tinggi: ");
            t = in.nextDouble();
            SegitigaParameter segitigaParameter = new SegitigaParameter(a, t);
        }
    }
}
