package id.hilmibill01.pertemuan.ketujuh;

/**
 *
 * @author Hilmi
 */
public class Kabisat {

    public boolean getHasil(int tahun) {
        if ((tahun % 4 == 0) && (tahun % 100 != 0) || (tahun % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }
}
