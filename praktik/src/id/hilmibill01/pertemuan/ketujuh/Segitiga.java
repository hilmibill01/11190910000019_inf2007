package id.hilmibill01.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class Segitiga {
    
    double alas, tinggi, luas;

    public Segitiga() {
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan alas: ");
        alas = in.nextDouble();
        System.out.print("masukkan tinggi: ");
        tinggi = in.nextDouble();
        luas = (alas * tinggi) / 2;
        System.out.println("Luas: " + luas);
    }
}
