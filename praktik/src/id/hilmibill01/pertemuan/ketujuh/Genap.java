package id.hilmibill01.pertemuan.ketujuh;

/**
 *
 * @author Hilmi
 */
public class Genap {

    public boolean getHasil(int bilangan) {
        return (bilangan % 2 == 0);
    }
}
