package id.hilmibill01.pertemuan.ketujuh;

/**
 *
 * @author Hilmi
 */
public class PersegiPanjang {

    private double panjang;
    private double lebar;

    public PersegiPanjang() {
        System.out.println("Konstruksi Persegi Panjang");
    }

    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public double getLuas() {
        return panjang * lebar;
    }

    public double getKeliling() {
        return 2 * (panjang + lebar);
    }

    public void getInfo() {
        System.out.println("kelas persegi panjang");
    }
}
