package id.hilmibill01.pertemuan.ketiga;
import java.util.Scanner;

/**
 *
 * @author Hilmi
 */

public class TukaranPecahan {
    public static void main(String[] args) {
    int jumlahUang, pecahan1, pecahan2, pecahan3, pecahan4, pecahan5, sisa;
    Scanner in = new Scanner(System.in);
        System.out.print("jumlah uang : ");
        jumlahUang = in.nextInt();
        
        pecahan1 = jumlahUang / 1000;
        sisa = jumlahUang % 1000;
        pecahan2 = sisa / 500;
        sisa = sisa % 500;
        pecahan3 = sisa / 100;
        sisa = sisa % 100;
        pecahan4 = sisa / 50;
        sisa = sisa % 50;
        pecahan5 = sisa / 25;
     
        System.out.println("tukaran pecahannya");
        System.out.println("Rp. 1000 = " + pecahan1 + " buah");
        System.out.println("Rp. 500 = " + pecahan2 + " buah");
        System.out.println("Rp. 100 = " + pecahan3 + " buah");
        System.out.println("Rp. 50 = " + pecahan4 + " buah");
        System.out.println("Rp. 25 = " + pecahan5 + " buah");
    }
}
