package id.hilmibill01.pertemuan.ketiga;

/**
 *
 * @author Hilmi
 */
import java.util.Scanner;

public class SelisihDuaTanggal {
    public static void main(String[] args) {
        int tgl1, bln1, thn1, tgl2, bln2, thn2, tgl3, bln3, thn3, selisih;
        Scanner in = new Scanner (System.in);
        System.out.println("tgl 1 : ");
        tgl1 = in.nextInt();
        System.out.println("bln 1 : ");
        bln1 = in.nextInt();
        System.out.println("thn 1 : ");
        thn1 = in.nextInt();
        System.out.println("tgl 2 : ");
        tgl2 = in.nextInt();
        System.out.println("bln 2 : ");
        bln2 = in.nextInt();
        System.out.println("thn 2 : ");
        thn2 = in.nextInt();
        
        selisih = (thn2 - thn1) * 365 + (bln2-bln1) * 30 + (tgl2 -tgl1);
        thn3 = selisih / 365;
        bln3 = (selisih % 365) / 30;
        tgl3 = (selisih % 365) % 30;
        
        System.out.println("selisih");
        System.out.println(thn3 + " thn " + bln3 + " bln " + tgl3 + " hari ");
    }
}
