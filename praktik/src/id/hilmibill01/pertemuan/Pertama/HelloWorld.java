package id.hilmibill01.pertemuan.pertama;
/* PROGRAM HelloWorld */
/* Penjelasan untuk mencetak "Hello World". Masukan progrsm ini tidak
 ada. Keluarannya adalah tulisan 'Hello, World' tercetak di layar */

public class HelloWorld {
    /* DEKLARASI */
    /* tidak ada */

    /* ALGORITMA */
    public static void main(String[] args) {
        System.out.print("Hello, world");
    }
}
