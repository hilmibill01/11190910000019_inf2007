package id.hilmibill01.pertemuan.keenam;
import java.util.Scanner;
/**
 *
 * @author Hilmi
 */
public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner (System.in);
        System.out.println("Masukkan bilangan: ");
        bilangan = in.nextInt();
        
        System.out.println("Bilangan: " + bilangan);
    }
}
