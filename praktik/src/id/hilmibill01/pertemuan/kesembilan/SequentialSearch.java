package id.hilmibill01.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class SequentialSearch {

    public boolean getSearchOutBoolean(int L[], int n, int x) {
        int i;

        i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("posisi ke " + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("posisi ke " + i + " isinya " + L[i]);
        }

        if (L[i] == x) {
            return true;
        } else {
            return false;
        }
    }

    public int getSearchOutIndeks(int L[], int n, int x) {
        int i;

        i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            i = i + 1;
        }
        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public boolean getSearchInBoolean(int L[], int n, int x) {
        int i;
        boolean ketemu;

        i = 0;
        ketemu = false;
        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
            }
        }
        return ketemu;
    }

    public int getSearchInIndeks(int L[], int n, int x) {
        int i;
        boolean ketemu;

        i = 0;
        ketemu = false;
        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
            }
        }

        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public int getSearchSentinel(int L[], int n, int x) {
        int i, index;

        L[n] = x;
        i = 0;

        while (L[i] != x) {
            i = i + 1;
        }

        if (i == n) {
            index = -1;
        } else {
            index = i;
        }
        return index;
    }

    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
//        int L[] = new int[7];
//        L[0] = 13;
//        L[1] = 16;
//        L[2] = 14;
//        L[3] = 21;
//        L[4] = 76;
//        L[5] = 15;
        int x;
        int n = 6;

        SequentialSearch app = new SequentialSearch();
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan x : ");
        x = in.nextInt();

        System.out.println("index = " + app.getSearchOutBoolean(L, n, x));
        //System.out.println("index = " + app.getSearchOutIndeks(L, n, x));
        //System.out.println("index = " + app.getSearchInBoolean(L, n, x));
        //System.out.println("index = " + app.getSearchInIndeks(L, n, x));
        //System.out.println("index = " + app.getSearchSentinel(L, n, x));
    }
}
