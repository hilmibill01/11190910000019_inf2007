package id.hilmibill01.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class OtherSearch {
    
    public int getOtherSearch (int L[], int x) {
        int i, index = -1;
        
        for (i = 0; i < L.length; i++) {
            if (L[i] == x) {
                index = i;
            }           
        }
        return index;
    }
    
    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
        int x, n = 6;
        
        Scanner in = new Scanner(System.in);
        OtherSearch app = new OtherSearch();
        
        System.out.print("masukkan x: ");
        x = in.nextInt();
        
        System.out.println("index = " + app.getOtherSearch(L, x));
    }
}
