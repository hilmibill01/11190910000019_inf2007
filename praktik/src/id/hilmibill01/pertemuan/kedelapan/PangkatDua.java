package id.hilmibill01.pertemuan.kedelapan;

/**
 *
 * @author Hilmi
 */
public class PangkatDua {

    public static void main(String[] args) {
        int pangkat[] = new int[10];
        int k, i;

        k = 0;
        for (i = 0; i < pangkat.length; i++) {
            k = i + 1;
            pangkat[i] = k * k;
            System.out.println("pangkat dua dari angka " + (i + 1)
                    + " = " + pangkat[i]);
        }
    }
}
