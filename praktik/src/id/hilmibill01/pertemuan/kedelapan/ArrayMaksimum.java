package id.hilmibill01.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class ArrayMaksimum {

    int i, maks;

    public int getMaks(int A[], int n) {
        maks = -9999;
        for (i = 0; i < n; i++) {
            if (A[i] > maks) {
                maks = A[i];
            }
        }
        return maks;
    }

    public static void main(String[] args) {

        int i, n;

        ArrayMaksimum arrayMaksimum = new ArrayMaksimum();
        Scanner in = new Scanner(System.in);

        System.out.print("N = ");
        n = in.nextInt();

        int A[] = new int[n];

        for (i = 0; i < A.length; i++) {
            System.out.print("masukkan array [" + i + "] : ");
            A[i] = in.nextInt();
        }
        System.out.println("elemen terbesar = " + arrayMaksimum.getMaks(A, n));
    }
}
