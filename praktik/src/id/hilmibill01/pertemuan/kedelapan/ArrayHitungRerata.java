package id.hilmibill01.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Hilmi
 */
public class ArrayHitungRerata {

    public static void main(String[] args) {

        int x[] = new int[6];
        int i;
        double jumlah, u;
        Scanner in = new Scanner(System.in);

        for (i = 0; i < x.length; i++) {
            System.out.print("masukkan array [" + i + "] : ");
            x[i] = in.nextInt();
        }

        System.out.print("\nnilai yang dimasukkan : ");
        for (i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }

        jumlah = 0;
        for (i = 0; i < x.length; i++) {
            jumlah = jumlah + x[i];
        }

        u = jumlah / 6;
        System.out.println("");
        System.out.println("Rata-Rata = " + u);
    }
}
