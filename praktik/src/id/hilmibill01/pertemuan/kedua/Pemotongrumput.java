package id.hilmibill01.pertemuan.kedua;

import java.util.Scanner;

public class Pemotongrumput {
    public static void main(String args[]) {
        double a, b, c, d, e, f;
        Scanner in = new Scanner(System.in);
        a = in.nextInt(); //Panjang tanah
        b = in.nextInt(); //Lebar tanah
        c = in.nextInt(); //Panjang rumah
        d = in.nextInt(); //Lebar rumah
        e = 2.5; //Kecepatan
        
        f = ((a*b)-(c*d))/e;
        
        System.out.println("waktu yang dibutuhkan = " + f + " menit");
    }
    
}
