package id.hilmibill01.pertemuan.kedua;

/**
 *
 * @author Hilmi
 */
public class Octal {
    public static void main(String args[]) {
        int six = 06;
        int seven = 07;
        int eight = 010;
        int nine = 011;
        System.out.println("octal six = " + six);
        System.out.println("Octal seven = " + seven);
        System.out.println("Octal eight = " + eight);
        System.out.println("Octal nine = " + nine);
        
    }
}
